

/*var bcrypt = require('bcryptjs');
bcrypt.hash('admin', 10, function(err, hash){
  console.log('admin', err, hash);
});*/

var express = require('express');
var app = express();
var path = require('path');
var _host = '127.0.0.1';
var port = 3000;

app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

var server = app.listen(port, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://'+_host+':'+port);
});