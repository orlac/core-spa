var gulp = require("gulp");
var webpack = require("webpack");
var BowerWebpackPlugin = require('bower-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
// var webpack = require('gulp-webpack-build');

var $ = require('gulp-load-plugins')({
    camelize: true
});

module.exports = function(options) {
    // gulp.task('webserver', function() {
    //     gulp.src(['./', '!./node_modules/**', '!./vendors/**'])
    //         .pipe($.webserver({
    //             livereload: true,
    //             directoryListing: false,
    //             open:false,
    //             fallback: 'index.html'
    //         }));
    // });

    gulp.task("webpack", function() {
        return gulp.src( options.src + '/app/index.js' )
            .pipe($.webpack({
                cache: true,
                target: "web",
                debug: true,
                watch: false,
                devtool: "#inline-source-map",
                // devtool: "#source-map",
                resolve: {
                    modulesDirectories: [
                        options.bower_components
                        ,options.node_modules
                    ],
                    alias: {
                        "angular": "angular/angular.min",
                        "ui-router": "angular-ui-router/release/angular-ui-router.min",
                        "angular-animate": "angular-animate/angular-animate.min",
                        "angular-touch": "angular-touch/angular-touch.min",
                        "angular-messages": "angular-messages/angular-messages.min",
                        "angular-breadcrumb": "angular-breadcrumb/dist/angular-breadcrumb.min",
                        "angular-mocks": "angular-mocks/angular-mocks",
                        "jquery": "jquery/dist/jquery.min",
                        "moment": "moment/min/moment.min",
                        // "socket.io": "socket.io/lib/min/moment.min",
                        "react": "react/react.min"
                    }
                },
                plugins: [
                    // new webpack.ResolverPlugin([
                    //     new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin("bower.json", ["main"])
                    // ]),
                    new BowerWebpackPlugin({
                        modulesDirectories: [
                            options.bower_components
                            ,options.node_modules
                        ],
                        manifestFiles: ['bower.json', '.bower.json'],
                        includes: /.*/,
                        excludes: /.*\.less$/
                    }),
                    new ExtractTextPlugin("bundle.css", {
                        allChunks: true
                    }),
                    // new webpack.optimize.UglifyJsPlugin({
                    //     compress: {
                    //         warnings: false
                    //     },
                    //     minimize: true
                    // })
                ],
                module: {
                    loaders: [
                        //{ test: /\.html$/, loader: "ng-cache?prefix=[dir]/[dir]" },
                        {test: /angular.min.js$/, loader: "exports?angular"},
                        //{ test: "/jquery.min.js$/", loader: "imports?jQuery=jquery" },
                        {test: /.es6.js$/, loader: '6to5-loader'},
                        //{test: /\.css$/, loader: 'style-loader!css-loader'},
                        {
                            test: /\.css$/,
                            loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
                        },
                        {
                            test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif|woff(\?.+?)|woff2(\?.+?)|eot(\?.+?)|ttf(\?.+?)|svg(\?.+?))$/,
                            loader: 'url-loader?limit=30000&name=[name]-[hash].[ext]'
                        },
                        
                    ]
                },
                // entry: {
                //     squadfree: options.src+"/squadfree",
                // },
                output: {
                    filename: "bundle.js",
                    // sourceMapFilename: 'bundle.js.map'
                    //path: options.dist+'/'
                }
            }))
            .pipe(gulp.dest(options.publicdist+'/build/'));
    });

    // gulp.task("default", [
    //     //"webserver"
    //     ,"webpack"
    // ]);
}


