describe('auth', function () {
    
    var $httpBackend;
    var $window;
    var apiService;
    var userFactory;
    var apiServiceMockModule;
    var apiMock;

    var injector;

    beforeEach( module(/*'application1',*/ 'mockModule', 'apiServiceModule', 'router') );
    

    beforeEach(function () {
        
        inject(function ($injector) {
            injector = $injector;

            
            $httpBackend = $injector.get('$httpBackend');
            apiMock = $injector.get('apiMock');
            apiMock.setStorageObj({});

            userFactory = $injector.get('userFactory');
            apiService = $injector.get('apiService', {'userFactory': userFactory});
            

            

            // $httpBackend.whenGET('/api/user/get').respond(apiMock.api_user_get);
            // $httpBackend.whenGET('/api/users/get').respond(apiMock.api_users_get);
            
        });
    });

    afterEach(function () {
        // $httpBackend.flush();
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('access', function () {
        $httpBackend.whenGET('/api/user/get').respond(apiMock.api_user_get);
        apiService.go('cabinetHome');
        expect(userFactory.user).toEqual(null);
    });

    var _login = function(role){
        apiService.userAuth({
            username: role,
            password: role
        });
        $httpBackend.flush();
    }

    it('login Admin', function () {

        $httpBackend.whenPOST('/auth/login').respond(apiMock.auth_login);
        $httpBackend.whenGET('/api/user/get').respond(apiMock.api_user_get);

        _login('admin');

        
        console.log("userFactory.user", userFactory.user); 
        var _r = (userFactory.user)? true : false ;
        expect(_r).toEqual(true);
        expect(userFactory.user.role).toEqual('admin');

        apiService.ensureAccess();
        $httpBackend.flush();
        var _r = (userFactory.user)? true : false ;
        expect(_r).toEqual(true);

    });    

    it('login User', function () {

        $httpBackend.whenPOST('/auth/login').respond(apiMock.auth_login);
        $httpBackend.whenGET('/api/user/get').respond(apiMock.api_user_get);

        _login('user');

        
        console.log("userFactory.user", userFactory.user); 
        var _r = (userFactory.user)? true : false ;
        expect(_r).toEqual(true);
        expect(userFactory.user.role).toEqual('user');

        apiService.ensureAccess();
        $httpBackend.flush();
        var _r = (userFactory.user)? true : false ;
        expect(_r).toEqual(true);

    });

    it('logout User', function () {

        userFactory.user = {id: 1};
        $httpBackend.whenPOST('/auth/logout').respond(apiMock.auth_logout);
        apiService.userLogout();
        $httpBackend.flush();
        var _r = (userFactory.user)? true : false ;
        console.log("userFactory.user", userFactory.user); 
        expect(_r).toEqual(false);

    });

});