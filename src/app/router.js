'use strict'

require('./services/pageService.js');
require('./controllers/homeCtrl.js');
require('./controllers/cabinetSettingsCtrl.js');

angular.module('router', ['ui.router', 'home', 'cabinetSettings', 'pageServiceModule'])
	.config(function($locationProvider, $stateProvider, $urlRouterProvider) {

    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });
    
    $urlRouterProvider.otherwise("/404");
	
	$stateProvider
	.state('home', {
		url: "/",
        template: require('ng-cache!./templates/home.html'),
        controller: function(pageService) {
            pageService.setTitle('Home');
        }
	})
    .state('cabinetHome', {
        url: "/cabinet",
        template: require('ng-cache!./templates/cabinet.html'),
        controller: function(pageService) {
            pageService.setTitle('Cabinet');
        }
    })    
    .state('cabinetSettings', {
        url: "/cabinet/settings",
        template: require('ng-cache!./templates/cabinet_settings.html'),
        controller: function(pageService) {
            pageService.setTitle('Настройки');
        }
    })
	.state('404', {
		url: "/404",
        template: require('ng-cache!./templates/404.html'),
        controller: function(pageService) {
            pageService.setTitle('Ничего не найдено');
        }
	});

	});