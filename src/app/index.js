'use strict';
(function(window){

	var bower_components = '../bower_components';

	window.jQuery = window.$ = require('../../bower_components/jquery/dist/jquery.min.js');
	require('angular');
	require('ui-router');
	require('angular-touch');
	require('angular-mocks');
	window._ = require('../../bower_components/lodash/lodash.min.js');
	

	require('../../bower_components/angular/angular-csp.css');
	
	// var jQuery = $;
	//window.jQuery = require("jquery");
	//window.$ = window.jQuery

	/* theme */
	// require('../fonts1.css');
	// require('../fonts2.css');
	require('../../bower_components/font-awesome/css/font-awesome.min.css');
	// require('../devAid-v1.0/assets/css/styles.css');
	
	
	require('../../bower_components/bootstrap/dist/css/bootstrap.min.css');
	require('../../bower_components/bootstrap/dist/js/bootstrap.min.js');
	require('../../bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js');
	require('../../bower_components/angular-socket-io/socket.min.js');
	
	require('../../bower_components/ngstorage/ngStorage.min.js');

	

	/* theme */
	// require('../squadfree/js/wow.min.js');
	// require('../devAid-v1.0/assets/js/main.js');
	
	/* icons */
	// require('../iconpack.css');
	require('../common.css');
	require('../dashboard.css');



	// require('../../bower_components/bootstrap-material-design/dist/css/material.min.css');
	// require('../../bower_components/bootstrap-material-design/dist/css/material-fullpalette.min.css');
	// require('../../bower_components/bootstrap-material-design/dist/css/ripples.min.css');
	// require('../../bower_components/bootstrap-material-design/dist/js/ripples.min.js');
	// require('../../bower_components/bootstrap-material-design/dist/js/material.min.js');

	require('./router.js');
	require('./top_sidebar/directive.js');
	require('./left_sidebar/directive.js');
	require('./notifyService/notifyService.js');
	require('./services/apiService.js');
	require('./services/apiServiсeMock.js');

	// require('./home/homeCtrl.js');
	

	var ng = angular.module('application1', [
		'router'
		, 'ngTouch'
		, 'ui.bootstrap'
		, 'topSidebar'
		, 'leftSidebar'
		, 'apiServiceModule'
		, 'apiServiceMockModule'
		, 'notifyServiceModule'
		// , 'home'
	]);

	angular.element(document).ready(function() {
	    angular.bootstrap(document, [ng.name]);
	});


})(window);




