'use strict';
(function(){
	
	var _fn = function(apiService){
                return {
        		restrict: 'AE',
                        replace: true,
                	scope: {
                		logined: '@'
                	},
                	controller: function($scope, $element, $attrs){
                		
                                $scope.apiService = apiService;

                                $scope.isLogined = function(){
                			return $scope.logined > 0;
                		}

                                $scope.getClassState = function(_item, state){
                                        console.log('state', _item, state);
                                }
                	},
                	template: require('ng-cache!./template.html'),
                	// template: '<p>test</p>',
		};
	};

	angular.module('topSidebar', ['ui.router', 'apiServiceModule'])
	.directive('topSidebar',  [ 'apiService', _fn] )	
})();
