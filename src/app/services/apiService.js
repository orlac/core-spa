'use strict';

var ng = angular.module('apiServiceModule', ['notifyServiceModule', 'ui.router'/*, 'apiServiceMockModule'*/]);


ng.factory('userFactory', function(){
  return {
    
    user: null,
    users: [],

    getIsAdmin: function(){
      return (this.user)? this.user.role == 'admin' : false;
    }
  }
})

ng.factory('apiService', [
  '$q', '$state', '$window', '$location', 'notifyService', '$http', 'userFactory',/*'$state',*/ 
  function($q, $state, $window, $location, notifyService, $http, userFactory /*, $state*/){
		

		var publish = {

          ensureAccessNotLogin: function(){
            var me = this;
            this.getUser(function(){
                me.go('cabinetHome')
            }, function(){});  
          },
          ensureAccess: function(role){
            var me = this;
            var data = {};
            this._get_user(data, function(user){
              if(role && (user.role != role)  ){
                me.goout( err.go || '/')
                notifyService.error( err.message || 'Доступ запрещен' );
              }
            }, function(err){ 
                me.goout( err.go || '/')
                notifyService.error( err.message || 'Доступ запрещен' );
            });  
          },

          userAuth: function(data){
              var me = this;
              $http.post('/auth/login', data)
                .success(function(res, status, headers, config){
                    userFactory.user = res;
                    console.log('/auth/login', res);
                    me.go('cabinetHome');
                })
                .error(function(res, status, headers, config){
                  console.log('/auth/login', res);
                  notifyService.error( res.message  || 'что-то пошло не так...' )
                });
          },

          userLogout: function(){
              var me = this;
              $http.post('/auth/logout')
                .success(function(res, status, headers, config){
                    userFactory.user = res;
                    console.log('/auth/logout', res);
                    me.go('home');
                })
                .error(function(res, status, headers, config){
                  console.log('/auth/logout', res);
                  notifyService.error( res.message  || 'что-то пошло не так...' )
                });
          },

          _get_user: function(data, _ok, _err){
              var me = this;

              $http.get('/api/user/get', {params: data})
                .success(function(res, status, headers, config){
                    userFactory.user = res;
                    console.log('/api/user/get', res);
                    if(_ok){
                      _ok(res);
                    }
                })
                .error(function(res, status, headers, config){
                    userFactory.user = null;
                    console.log('/api/user/get ', res);
                    if(_err){
                      _err(res);
                    }else{
                      notifyService.error( res.message || 'что-то пошло не так...' )
                    }
                });
          },

          getUser: function(_ok, _err){
            var me = this;
            me._get_user(null, _ok, _err);
          },

          

          saveSelfUser: function(user){
              var self = this;
              var deferred = $q.defer();  
              $http.post('/api/selfuser', user)
                .success(function(res, status, headers, config){
                    console.log('/api/selfuser', res);
                    deferred.resolve(res);
                })
                .error(function(err, status, headers, config){
                    console.log('/api/selfuser', err);
                    deferred.reject(err);
                    notifyService.error( err.message || 'что-то пошло не так...' )
                });
              return deferred.promise; 
          },

          deleteUser: function(id){
              var self = this;
              var deferred = $q.defer();  
              $http.delete('/api/user', {data: {id: id} } )
                .success(function(res, status, headers, config){
                    console.log('/api/user', res);
                    self.getUsers();
                    deferred.resolve(res);
                })
                .error(function(err, status, headers, config){
                    console.log('/api/user', err);
                    deferred.reject(err);
                    notifyService.error( err.message || 'что-то пошло не так...' )
                });
              return deferred.promise; 
          },

          goout: function(path){
            $window.location.href = path;
          },
          go: function(path){
            //todo костыльно это
            // scope.document.getElementById('router').go(path, {replace: true})
            // var href = $urlRouter.href( path );
            $state.go(path);
            // $window.location.href = path;
            // $location.path(path);
          }
      	};

      	return publish;
}]);