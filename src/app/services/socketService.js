'use strict';

angular.module('socketServiceModule', ['btford.socket-io'])
	.provider('socketConfig', function () {
		var config = {
			host: '',
			port: ''
		};

		this.setConfig = function(value) {
			config = value;
		};

		this.$get = function () {
			return config;
		};
	})
	.factory('socketService', ['socketFactory', '$window', 'socketConfig', function(socketFactory, $window, socketConfig){
		
	    var url = socketConfig.host+':'+socketConfig.port+socketConfig.path
		return socketFactory({
			prefix: socketConfig.path,
			// ioSocket: io.connect( $window.location.hostname+':3002/socket', {transports: ['websocket']})
			ioSocket: io.connect( url, {transports: ['websocket']})
		});
		
	}]);