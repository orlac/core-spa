'use strict';

var mockModule = angular.module('mockModule', []);
mockModule.provider('apiMock', [ function(){
  
  var $localStorage = null;

  var user = null;
  var users = [
    {
      id: 1, 
      user_auths: { username: 'user' }
    }
  ];

  var api_user_get = function(method, url, data){
    if($localStorage.user){
      return [200, $localStorage.user]
    }else{
      return [403, {go: '/', message: 'Нужно авторизоваться'}];   
    }
  };

  var api_users_get = function(method, url, data){
    $localStorage.users = $localStorage.users || users;
    if($localStorage.users){
      return [200, $localStorage.users]
    }else{
      return [403, {go: '/', message: 'Нужно авторизоваться'}];   
    }
  }

  var auth_login = function(method, url, data){
    data = JSON.parse(data);
    if( data.username == 'admin' && data.password == 'admin'){
      user = {
        id: 1,
        role: 'admin',
        active: 1,
        user_auths: {
          username: 'admin',
          password: 'admin'
        }
      }
      $localStorage.user = user;
      return [200, user];   
    }else if(data.username == 'user' && data.password == 'user'){
      user = {
        id: 2,
        role: 'user',
        active: 1,
        user_auths: {
          username: 'user',
          password: 'user'
        }
      }
      $localStorage.user = user;
      return [200, user];   
    }else{
      return [403, {message: 'Пользователь не найден' } ];
    }
  }

  var api_selfuser = function(method, url, data){
    data = JSON.parse(data);

    if( data.user_auths.password == $localStorage.user.user_auths.password && data.new_password){
      $localStorage.user.user_auths.password = data.new_password
      return [200, $localStorage.user];   
    }else{
      return [400, {message: 'Неверный ввод' } ];
    }

  }

  var api_user_post = function(method, url, data){
    data = JSON.parse(data);
    if((data.user_auths) &&  ( data.user_auths.username && data.user_auths.password ) ){
      $localStorage.users = $localStorage.users || users;
      if(data.id){
        for(var i in $localStorage.users){
          if($localStorage.users[i].id == data.id){
            $localStorage.users[i] = data;
          }
        }
      }else{
        var _user = data;
        _user.id = $localStorage.users.length;
        _user.role = 'user';
        _user.active = 1;
        
        $localStorage.users.push(_user);
      }
      
      return [200];

    }else{
      return [400, {message: 'Неверный ввод' } ];
    }
  };

  var api_user_delete = function(method, url, data){
    data = JSON.parse(data);
    if(data.id ){
      $window._.remove($localStorage.users, function(item){
        return item.id == data.id;
      });
      return [200];
    }else{
      return [400, {message: 'Неверный ввод' } ];
    }
  }

  var auth_logout = function(method, url, data){
    $localStorage.user = null;
    return [200];   
  }

  var setStorageObj = function(storageObj){
    $localStorage = storageObj;
  }

  this.$get = function(){
    return {
      api_user_get: api_user_get
      ,api_users_get: api_users_get
      ,auth_login: auth_login
      ,api_selfuser: api_selfuser
      ,api_user_post: api_user_post
      ,api_user_delete: api_user_delete
      ,auth_logout: auth_logout


      ,setStorageObj: setStorageObj
    }
  }
}]);



var ng = angular.module('apiServiceMockModule', ['ngMockE2E', 'ngStorage', 'mockModule']);


ng.run(['$httpBackend', '$localStorage', '$window', 'apiMock', function($httpBackend, $localStorage, $window, apiMock){

  var user = null;
  var users = [
    {
      id: 1, 
      user_auths: { username: 'user' }
    }
  ];

  apiMock.setStorageObj($localStorage);
  
  $httpBackend.whenGET('/api/user/get').respond(apiMock.api_user_get);

  $httpBackend.whenGET('/api/users/get').respond(apiMock.api_users_get);

  $httpBackend.whenPOST('/auth/login').respond(apiMock.auth_login);

  $httpBackend.whenPOST('/api/selfuser').respond(apiMock.api_selfuser);

  $httpBackend.whenPOST('/api/user').respond(apiMock.api_user_post);

  $httpBackend.whenDELETE('/api/user').respond(apiMock.api_user_delete);  

  $httpBackend.whenPOST('/auth/logout').respond(apiMock.auth_logout);

}])


