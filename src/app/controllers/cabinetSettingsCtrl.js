'use strict';

var ctrl = function($scope, apiService, notifyService, userFactory, $window){
	apiService.ensureAccess();
	
	
	this.apiService = apiService;
	this.userFactory = userFactory;

	
	this.user = userFactory.user;
	// this.mail = userFactory.mail;

	this.saveUser = function(){
		var self = this;
		apiService.saveSelfUser(this.user).then(function(){
			notifyService.notify('Сохранено');
		});
	}

	// this.saveMail = function(id){
	// 	if($window.confirm('Удалить запись?')){
	// 		apiService.deleteUser(id);
	// 	}
	// }

	

};

angular.module('cabinetSettings', ['topSidebar', 'leftSidebar', 'apiServiceModule', 'notifyServiceModule'])
	.controller('cabinetSettingsCtrl', [
		'$scope'
		,'apiService'
		,'notifyService'
		,'userFactory'
		,'$window'
		, ctrl
	]);